document.body.innerHTML = `<div id="wrapper">
<div id="story">Þetta er sagan um kirkjur</div>
<button id="next">Næsti kafli</button>
</div>`;
const {getChapter, getTitle} = require('../main.js');



test('Title should have two or more letters', () => {
    for(let i = 0; getTitle(i); i++){
        expect(getTitle(i).length).toBeGreaterThanOrEqual(2);
      }
});

test('Every title of a chapter in the story should include the word "kafli"', () => {
  for(let i = 0; getTitle(i); i++){
    expect(getTitle(i)).toMatch(/kafli/);
  }
});

test('Chapter should not be a number', () => {
    for(let i = 0; getChapter(i); i++){
      expect(getChapter(i)).not.toBeNaN();
    }
  });

  test('Chapter should be defined', () => {
    for(let i = 0; getChapter(i); i++){
      expect(getChapter(i)).toBeDefined();
    }
  });

  
  
  

  